import React from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { increase, decrease } from 'actions';
import { Section, Container } from 'components/common/styledComponents';
import { selectCount } from 'selectors/counter';
import Counter from 'components/counter';

const Index = props => (
	<Section>
		<Container>
			<Counter {...props} />
		</Container>
	</Section>
);

Index.getInitialProps = ({ store }) => {
	store.dispatch(increase());
};

export default connect(
	createSelector(selectCount(), count => ({ count })),
	{ increase, decrease }
)(Index);
