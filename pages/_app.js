import React from 'react';
import { Provider } from 'react-redux';
import App, { Container } from 'next/app';
import withRedux from 'next-redux-wrapper';
import makeStore from 'utils/store';
import { GlobalStyles } from 'components/common/styledComponents';

class NextStaterApp extends App {
	static async getInitialProps({ Component, ctx }) {
		return {
			pageProps: {
				...(Component.getInitialProps ? await Component.getInitialProps(ctx) : {})
			}
		};
	}

	render() {
		const { Component, pageProps, store } = this.props;

		return (
			<Container>
				<Provider store={store}>
					<>
						<GlobalStyles />
						<Component {...pageProps} />
					</>
				</Provider>
			</Container>
		);
	}
}

export default withRedux(makeStore, { debug: false })(NextStaterApp);
