import { createSelector } from 'reselect';

const selectCounter = () => state => state.counter;

export const selectCount = () =>
	createSelector(selectCounter(), counterState => counterState.count);
