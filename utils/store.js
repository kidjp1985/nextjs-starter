import thunk from 'redux-thunk';
import { createStore, applyMiddleware, compose } from 'redux';
import reducer from 'reducers';

export default initialState => {
	const middlewares = [thunk];

	if (process.env.NODE_ENV === 'development') {
		const { logger } = require('redux-logger');

		middlewares.push(logger);
	}

	return createStore(reducer, initialState, compose(applyMiddleware(...middlewares)));
};
