import { INCREMENT, DECREMENT } from 'constants/actionTypes';

export const increase = () => ({ type: INCREMENT });

export const decrease = () => ({ type: DECREMENT });
