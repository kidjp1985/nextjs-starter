const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');

module.exports = {
	webpack: (config, { dev }) => {
		/* Enable only in Production */
		if (!dev) {
			// Service Worker
			config.plugins.push(
				new SWPrecacheWebpackPlugin({
					cacheId: 'next-ss',
					filename: 'sw.js',
					minify: true,
					staticFileGlobsIgnorePatterns: [/\.next\//],
					staticFileGlobs: [
						'static/**/*' // Precache all static files by default
					],
					runtimeCaching: [
						{
							handler: 'fastest',
							urlPattern: /[.](png|jpg|css)/
						},
						{
							handler: 'networkFirst',
							urlPattern: /^http.*/
						}
					]
				})
			);
		}

		return config;
	},
	exportPathMap: () => {
		return {
			'/': { page: '/' }
		};
	}
};
