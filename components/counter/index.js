import React, { Fragment } from 'react';
import { compose, pure } from 'recompose';

const Counter = ({ increase, decrease, count }) => (
	<Fragment>
		<span>Counter: {count}</span>
		<button onClick={decrease}>-</button>
		<button onClick={increase}>+</button>
	</Fragment>
);

export default compose(pure)(Counter);
