const express = require('express');
const next = require('next');
const path = require('path');
const compression = require('compression');

const dev = process.env.NODE_ENV !== 'production';
const app = next({ dir: '.', dev });
const handle = app.getRequestHandler();

app
	.prepare()
	.then(() => {
		const server = express();

		server.set('port', process.env.port || 3000);
		server.use(compression());
		server.get('/sw.js', (req, res) => {
			return res.sendFile(path.resolve('./.next/sw.js'));
		});

		server.get('*', (req, res) => {
			return handle(req, res);
		});

		server.listen(server.get('port'), err => {
			if (err) throw err;

			console.log(`> Ready on http://localhost:${server.get('port')}`); // eslint-disable-line no-console
		});
	})
	.catch(err => {
		console.error(err.slack); // eslint-disable-line no-console
		process.exit(1);
	});
