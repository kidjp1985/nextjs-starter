nextjs-starter
================================

> This simple stater lets you quickly set up a project with Nextjs

## Contents

- [Installation](#installation)
- [Development Workflow](#development-workflow)
- [Deployment](#deployment)

### Installation
Clone repo: 
```sh
git clone https://github.com/kidjp85/nextjs-stater.git
cd nextjs-starter
```

Make it your own:
```sh
rm -rf .git && git init && yarn init
```
> :information_source: This re-initializes the repo and sets up your project.

Install the dependencies:
```sh
yarn install
```
or
```sh
npm install
```

### Development Workflow
Start a live-reload development server:
```sh
yarn run dev
```
or
```sh
npm run dev
```

Generate a production build:
```sh
yarn run build
```
or
```sh
npm run build
```
### Deployment
[![Deploy to now](https://deploy.now.sh/static/button.svg)](https://deploy.now.sh/?repo=https://github.com/kidjp85/nextjs-starter)


### Contribution
I'm open to contributions & suggestions in making this a lot better :hand:

### License
MIT
